# FabricCB
Fabric CB (Circuit Board) is a simple tool for generating instructions for laser cutting conductive fabric traces in a fabric based circuit.

##.fcb format
The format for an input .fcb file is found in FCB.bnf. This bnf file is not used to read/write these files.
 A .fcb format follows the general format of:
 1. declaring the voltage available in the circuit (from the battery) 
 2. declaring each of the component nodes in the circuit and their current requirements
 3. The trace connections between each node based defined by the length of the trace and the maximum width the trace can be.
    + the split nodes (non-component nodes that splits a circuit into parrallel) must start with s followed by a number.
    
 ##Optimizing Trace Structures
 Calling the optimization function requires the input of a ciruit from a .fcb file and a set of possible conductors such as the fabrics set. 
 A call to the optimizeResistivity funciton will set the conductor and size of each of the traces in the ciruit. 
 ```java
CircuitTree circuit = CircuitTree.readCircuit("test.fcb");
ConductorSet fabrics = ConductorSet.fabricConductors();
circuit.optimizeResistivity(fabrics); 
 ```
Optimization occurs in 2 stages.
1. Each trace is given a target Resistiance based on a linear programming solution propegated from the leaves of the cicuit to the battery.
2. Each trace has its width and height and conductor set by a basic gradient descent which gradually increases height as needed and compares the cost function across all of the conductors in the set. 