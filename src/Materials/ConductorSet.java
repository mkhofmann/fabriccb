package Materials;

import com.sun.istack.internal.NotNull;

import java.util.HashMap;
import java.util.Iterator;
import java.util.TreeMap;

public class ConductorSet implements Iterable<Conductor>{
    //todo: tree map will only hold one value per rank but ranks may be equal.
    private TreeMap<Integer, Conductor> conductorsByRanking = new TreeMap<>();
    private HashMap<Conductor, Integer> ranksByConductor = new HashMap<>();
    private HashMap<Double, Double> minResistivityIndex = new HashMap<>();
    private HashMap<Double, Double> maxResistivityIndex = new HashMap<>();
    public ConductorSet(){

    }

    public double[] rankedSumVector(){
        double[] vector = new double[this.size()+1];
        Iterator<Conductor> conductorIterator = this.iteratorByRanking();
        Conductor conductor = conductorIterator.next();
        for(int i=0; i<vector.length-1; i++) {
            vector[i] = this.rankCost(ranksByConductor.get(conductor));
            if(conductorIterator.hasNext())
                conductor = conductorIterator.next();
        }
        vector[vector.length-1] = 0;
        return vector;
    }

    private int topRank(){
        return this.conductorsByRanking.lastKey();
    }

    private int rankCost(int consideredRank){
        assert consideredRank>=0;
        return this.topRank()-consideredRank+1;//add the 1 so that even the best fabric still has a cost, non-zero objective function
    }

    public int rankCost(Conductor conductor){
        return this.rankCost(this.ranksByConductor.get(conductor));
    }

    public double getMinResistivity(double angleInRads){
        if(minResistivityIndex.containsKey(angleInRads))
            return minResistivityIndex.get(angleInRads);
        double minResistivity = Double.MAX_VALUE;
        for(Conductor conductor: this){
            double resistivity = conductor.getResistivity(angleInRads);
            if(resistivity< minResistivity){
                minResistivity = resistivity;
            }
        }
        minResistivityIndex.put(angleInRads, minResistivity);
        return minResistivity;
    }
    public double getMaxResistivity(double angleInRads){
        if(maxResistivityIndex.containsKey(angleInRads))
            return maxResistivityIndex.get(angleInRads);
        double maxResistivity = 0.0;
        for(Conductor conductor: this){
            double resistivity = conductor.getResistivity(angleInRads);
            if(resistivity> maxResistivity){
                maxResistivity = resistivity;
            }
        }
        maxResistivityIndex.put(angleInRads, maxResistivity);
        return maxResistivity;
    }
    private void addConductor(int rank, @NotNull Conductor conductor){
        assert rank>=0;
        this.conductorsByRanking.put(rank, conductor);
        this.ranksByConductor.put(conductor, rank);
        for(double angleInRads: this.minResistivityIndex.keySet()){
            double resistivity = conductor.getResistivity(angleInRads);
            if(resistivity < minResistivityIndex.get(angleInRads))
                minResistivityIndex.put(angleInRads, resistivity);
            if(resistivity > maxResistivityIndex.get(angleInRads))
                maxResistivityIndex.put(angleInRads, resistivity);
        }
    }

    @Override
    public Iterator<Conductor> iterator() {
        return iteratorByRanking();
    }

    private Iterator<Conductor> iteratorByRanking() {
        return this.conductorsByRanking.values().iterator();
    }

    private int size(){
        return this.conductorsByRanking.size();
    }

    public static ConductorSet fabricConductors(){
        ConductorSet fabrics = new ConductorSet();
        fabrics.addConductor(1, knitFabric());
        fabrics.addConductor(2, jerseyFabric());
        fabrics.addConductor(3, wovenFabric());
        return fabrics;
    }

    private static Conductor knitFabric() {
        return new Conductor(10, 1, "knit");
    }

    private static Conductor jerseyFabric() {
        return new Conductor(2.4, 13.8, "jersey");
    }

    public static ConductorSet jersey(){
        ConductorSet fabrics = new ConductorSet();
        fabrics.addConductor(1, jerseyFabric());
        return fabrics;
    }

    public static ConductorSet knit(){
        ConductorSet fabrics = new ConductorSet();
        fabrics.addConductor(1, knitFabric());
        return fabrics;
    }

    public static ConductorSet woven(){
        ConductorSet fabrics = new ConductorSet();
        fabrics.addConductor(1, wovenFabric());
        return fabrics;
    }

    public static ConductorSet jerseyAndWoven(){
        ConductorSet fabrics = new ConductorSet();
        fabrics.addConductor(1, jerseyFabric());
        fabrics.addConductor(2, wovenFabric());
        return fabrics;
    }

    private static Conductor wovenFabric() {
        return new Conductor(0.00081, 0.00081, "woven");
    }
}
