package Materials;

public class Conductor implements Comparable<Conductor>{

    double resistancePerVoxelDy;
    double resistancePerVoxelDx;

    public String name;

    Conductor(double resistancePerVoxelDx, double resistancePerVoxelDy, String name) {
        assert resistancePerVoxelDx>0;
        assert resistancePerVoxelDy>0;
        this.resistancePerVoxelDy = resistancePerVoxelDy;
        this.resistancePerVoxelDx = resistancePerVoxelDx;
        this.name = name;
    }

    public double getResistivity(double angleInRads){
        return this.resistancePerVoxelDx *Math.sin(angleInRads)+ this.resistancePerVoxelDy *Math.cos(angleInRads);
    }
    public double getConductance(double angleInRads){
        return 1.0 / this.getResistivity(angleInRads);
    }

    @Override
    public int hashCode() {
        return this.name.hashCode();
    }

    @Override
    public int compareTo(Conductor o) {
        return Double.compare(this.getResistivity(0), o.getResistivity(0));
    }
}
