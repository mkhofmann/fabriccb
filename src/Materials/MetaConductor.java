package Materials;

import com.sun.istack.internal.NotNull;

import java.util.LinkedList;

public class MetaConductor extends Conductor {
    public MetaConductor(@NotNull LinkedList<Conductor> conductors, @NotNull String name) {
        super(metaResistivity(conductors, true), metaResistivity(conductors, false), name);
    }

    private static double metaResistivity(@NotNull LinkedList<Conductor> conductors, boolean dx){
        assert !conductors.isEmpty();
        double inverseSum = 0.0;
        for(Conductor conductor: conductors) {
            if(dx)
                inverseSum += 1.0 / conductor.resistancePerVoxelDx;
            else
                inverseSum += 1.0 / conductor.resistancePerVoxelDy;
        }
        return 1.0 / inverseSum;
    }
}
