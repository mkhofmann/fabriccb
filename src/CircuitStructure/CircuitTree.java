package CircuitStructure;

import Materials.ConductorSet;
import Nodes.BatteryNode;
import Nodes.ComponentNode;
import Nodes.SplitNode;
import Nodes.TraceNode;
import Traces.Trace;
import com.sun.istack.internal.NotNull;

import java.io.*;
import java.util.*;

public class CircuitTree implements Iterable<TraceNode>{
    HashMap<String, TraceNode> nodesByName = new HashMap<>();
    HashSet<Trace> traces= new HashSet<>();
    private TreeMap<Integer, HashSet<TraceNode>> nodesByDistanceFromBattery = new TreeMap<>();
    double voltage;

    public CircuitTree(double voltage) {
        assert voltage>0;
        this.voltage = voltage;
        BatteryNode batteryNode = new BatteryNode();
        this.nodesByName.put(batteryNode.name, batteryNode);
    }

    void setMaxHeight(@SuppressWarnings("SameParameterValue") int maxHeight){
        assert maxHeight>=1;
        for(Trace trace: traces)
            trace.setMaxHeight(maxHeight);
    }

    @Override
    public Iterator<TraceNode> iterator() {
        return new BreadthFirstIterator();
    }

    private class BreadthFirstIterator implements Iterator<TraceNode>{
        HashSet<TraceNode> currentSet = null;
        Iterator<TraceNode> currentSetIter = null;
        int currentDistanceIndex;
        BreadthFirstIterator(){
            for(TraceNode node: nodesByName.values())
                addNodeByLevel(node);
            currentDistanceIndex = nodesByDistanceFromBattery.lastKey();
            setCurrentSet();
        }
        @Override
        public boolean hasNext() {
            if(!currentSetIter.hasNext()){
                Integer lowerDistance = nodesByDistanceFromBattery.lowerKey(currentDistanceIndex);
                if(lowerDistance == null)
                    return false;
                else{
                    currentDistanceIndex = lowerDistance;
                    setCurrentSet();
                    return true;
                }
            }else
                return true;
        }

        private void setCurrentSet() {
            currentSet = nodesByDistanceFromBattery.get(currentDistanceIndex);
            currentSetIter = currentSet.iterator();
        }

        @Override
        public TraceNode next() {
            boolean flag = hasNext();
            assert  flag;
            return currentSetIter.next();
        }
    }

    private TraceNode addNode(@NotNull TraceNode node){
        this.nodesByName.put(node.name,node);
        if(node.priorTrace != null) addNodeByLevel(node);
        return node;
    }

    private void addNodeByLevel(@NotNull TraceNode node) {
        int distanceFromBattery = node.distanceToBattery();
        if (this.nodesByDistanceFromBattery.containsKey(distanceFromBattery))
            this.nodesByDistanceFromBattery.get(distanceFromBattery).add(node);
        else {
            HashSet<TraceNode> newnodes = new HashSet<>();
            newnodes.add(node);
            this.nodesByDistanceFromBattery.put(distanceFromBattery, newnodes);
        }
    }

    private void addTrace(@NotNull TraceNode startNode,@NotNull TraceNode endNode, double maxWidth, double length){
        Trace trace = new Trace(startNode, endNode,  maxWidth, length, 0.0);
        this.traces.add(trace);
        endNode.setPriorTrace(trace);
        startNode.setNextTrace(trace);
    }

    static CircuitTree readCircuit(@NotNull String fileName) throws IOException {
        File file = new File(fileName);
        FileReader fileReader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String firstLine = bufferedReader.readLine();
        assert firstLine != null;
        CircuitTree circuitTree = new CircuitTree(Integer.parseInt(firstLine));
        String whs = "\\s+";
        String suffixRegex = whs + "\\d+(.\\d+)?";
        String compName = "[a-zA-Z0-9]+";
        String nextLine= null;
        for(String line = bufferedReader.readLine(); line != null && line.matches(compName + suffixRegex); line= nextLine = bufferedReader.readLine()){
            String[] data = line.split(whs);
            String nodeName = data[0];
            double current = Double.parseDouble(data[1]);
            ComponentNode node = new ComponentNode(current, nodeName);
            assert !circuitTree.nodesByName.containsKey(node.name);
            circuitTree.addNode(node);

        }
        assert nextLine != null;
        HashMap<String, SplitNode> splits = new HashMap<>();
        for(String line = nextLine;
            line!= null;
            line= bufferedReader.readLine()){
            String splitName = "[sS]\\d+";
            String arrow = whs + "->"+whs;
            String manyNumberSuffix = "(\\s+\\d+(.\\d+)?)+(\\s+[a-zA-Z]+)?(\\s*)";
            boolean splitToComp = line.matches(splitName + arrow + compName + manyNumberSuffix);
            boolean splitToSplit = line.matches(splitName + arrow +splitName + manyNumberSuffix);
            boolean compToSplit = line.matches(compName+arrow+splitName+manyNumberSuffix);
            boolean compToComp = line.matches(compName + arrow +compName + manyNumberSuffix);
            boolean batteryToComp = line.matches(BatteryNode.BATTERY+ arrow+compName+manyNumberSuffix);
            boolean batteryToSplit = line.matches(BatteryNode.BATTERY+arrow+splitName+manyNumberSuffix);
            assert splitToComp || splitToSplit || compToComp || compToSplit ||  batteryToComp|| batteryToSplit;
            String[] data = line.split(whs);
            String startNodeName = data[0];
            String endNodeName = data[2];
            double length = Double.parseDouble(data[3]);
            double maxWidth = Double.parseDouble(data[4]);
            TraceNode startNode;
            TraceNode endNode;
            if((splitToSplit || batteryToSplit || compToSplit) && !splits.containsKey(endNodeName)) {
                endNode = circuitTree.addNode(new SplitNode(endNodeName));
                splits.put(endNodeName, (SplitNode) endNode);
            }else
                endNode = circuitTree.nodesByName.get(endNodeName);
            if((splitToComp || splitToSplit) && !splits.containsKey(startNodeName)) {
                startNode = circuitTree.addNode(new SplitNode(startNodeName));
                splits.put(startNodeName, (SplitNode) startNode);
            }
            else if(batteryToComp || batteryToSplit)
                startNode = circuitTree.nodesByName.get(BatteryNode.BATTERY);
            else
                startNode = circuitTree.nodesByName.get(startNodeName);
            circuitTree.addTrace(startNode, endNode, maxWidth, length);
        }
        bufferedReader.close();
        circuitTree.nodesByName.get(BatteryNode.BATTERY).nextTrace.getDescendentCurrent();
        return circuitTree;
    }
    void propagateResistanceOptimization(@NotNull ConductorSet conductors) throws Exception {
        for(TraceNode node: this) {
            if(node instanceof BatteryNode)
                continue;
            double minResistor = conductors.getMinResistivity(node.priorTrace.angleInRads);
            double maxResistor = conductors.getMaxResistivity(node.priorTrace.angleInRads);
            node.setPriorResistivity(this.voltage, minResistor, maxResistor);
        }
    }

    void optimizeResistivity(@NotNull ConductorSet conductors) throws Exception {
        this.propagateResistanceOptimization(conductors);
        for(Trace trace: this.traces)
            trace.setByTargetResistance(conductors);
    }

}
