package CircuitStructure;

import Materials.ConductorSet;
import Traces.Trace;
import org.junit.Test;

public class CircuitTreeTest{

    @Test
    public void testFitTraces() throws Exception {
        CircuitTree circuit = CircuitTree.readCircuit("test.fcb");
        ConductorSet fabrics = ConductorSet.fabricConductors();
        circuit.optimizeResistivity(fabrics);
        for(Trace trace: circuit.traces)
            System.out.println(trace.start.name+"->"+trace.end.name+
                    " R_e: "+ trace.targetResistance +
                    " C: "+trace.conductor.getResistivity(0) +
                    " W: "+trace.width+
                    " H: "+trace.height+"" +
                    " R: "+ trace.getResistance());
    }

    @Test
    public void testMetaTraces() throws Exception{
        CircuitTree circuit = CircuitTree.readCircuit("test.fcb");
        ConductorSet fabrics = ConductorSet.fabricConductors();
        circuit.optimizeResistivity(fabrics);
        for(Trace trace: circuit.traces){
             trace.buildTraceWithMetaMaterial(fabrics, .5);
        }
    }

    @Test
    public void testJerseyRivetedByMeta() throws Exception {
        CircuitTree circuit = CircuitTree.readCircuit("corner.fcb");
        ConductorSet fabrics = ConductorSet.jersey();
        circuit.propagateResistanceOptimization(fabrics);
        for(Trace trace: circuit.traces) {
            trace.buildTraceWithMetaMaterial(fabrics, .05);
            System.out.println(trace.start.name+"->"+trace.end.name+
                    " R_e: "+ trace.targetResistance +
                    " C: "+trace.conductor.getResistivity(0) +
                    " W: "+trace.width+
                    " H: "+trace.height+"" +
                    " R: "+ trace.getResistance());
        }

    }

    @Test
    public void testJerseyAndWovenRiveted() throws Exception {
        CircuitTree circuit = CircuitTree.readCircuit("corner.fcb");
        ConductorSet fabrics = ConductorSet.jerseyAndWoven();
        circuit.propagateResistanceOptimization(fabrics);
        for(Trace trace: circuit.traces) {
            trace.buildTraceWithMetaMaterial(fabrics, .05);
            System.out.println(trace.start.name+"->"+trace.end.name+
                    " R_e: "+ trace.targetResistance +
                    " R: "+ trace.getResistance());
        }

    }
    @Test
    public void testJerseySinglePieceByMeta() throws Exception {
        CircuitTree circuit = CircuitTree.readCircuit("corner.fcb");
        ConductorSet fabrics = ConductorSet.jersey();
        circuit.nodesByName.get("E").priorTrace.angleInRads = Math.PI / 2.0; // 90 degree turn
        circuit.propagateResistanceOptimization(fabrics);
        for(Trace trace: circuit.traces)
            trace.buildTraceWithMetaMaterial(fabrics, .05);
        for(Trace trace: circuit.traces)
            System.out.println(trace.start.name+"->"+trace.end.name+
                    " R_e: "+ trace.targetResistance +
                    " C: "+trace.conductor.getResistivity(0) +
                    " W: "+trace.width+
                    " H: "+trace.height+"" +
                    " R: "+ trace.getResistance());
    }

    @Test
    public void testJerseyRiveted() throws Exception{
        CircuitTree circuit = CircuitTree.readCircuit("corner.fcb");
        ConductorSet jersey = ConductorSet.jersey();
        circuit.setMaxHeight(4);
        circuit.optimizeResistivity(jersey);
        for(Trace trace: circuit.traces)
            System.out.println(trace.start.name+"->"+trace.end.name+
                    " R_e: "+ trace.targetResistance +
                    " A: " + trace.angleInRads+
                    " C: "+trace.conductor.name+" == " +trace.conductor.getResistivity(trace.angleInRads) +
                    " W: "+trace.width+
                    " H: "+trace.height+"" +
                    " R: "+ trace.getResistance());

    }
    @Test
    public void testJerseySinglePiece() throws Exception{
        CircuitTree circuit = CircuitTree.readCircuit("corner.fcb");
        ConductorSet jersey = ConductorSet.jersey();
        circuit.setMaxHeight(4);
        circuit.nodesByName.get("E").priorTrace.angleInRads = Math.PI / 2.0; // 90 degree turn
        circuit.optimizeResistivity(jersey);
        for(Trace trace: circuit.traces)
            System.out.print(trace.start.name+"->"+trace.end.name+
                    "\nideal Resistance: "+ (circuit.voltage/trace.current)+
                    "\ntarget Resistance: "+ trace.targetResistance +
                    "\nAngle: " + trace.angleInRads+
                    "\nConductor: "+trace.conductor.name+" == " +trace.conductor.getResistivity(trace.angleInRads) +
                    "\nWidth: "+trace.width +
                    "\nHeight: "+trace.height+
                    "\nResistvity: "+ trace.getResistance()+
                    "\nConductance: " + trace.getConductance()+
                    "\nResistance: "+ trace.getResistivity()+"\n");

    }
}