package Nodes;

import Traces.Trace;
import com.sun.istack.internal.NotNull;
import scpsolver.constraints.LinearBiggerThanEqualsConstraint;
import scpsolver.constraints.LinearEqualsConstraint;
import scpsolver.constraints.LinearSmallerThanEqualsConstraint;
import scpsolver.lpsolver.LinearProgramSolver;
import scpsolver.lpsolver.SolverFactory;
import scpsolver.problems.LinearProgram;

public class TraceNode implements Comparable<TraceNode>{
    public String name;
    public double current;
    public Trace priorTrace= null;
    public Trace nextTrace = null;

    TraceNode(double current, @NotNull String name){
        assert current>0;
        this.current = current;
        this.name = name;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    public void setPriorTrace(@NotNull Trace trace) {
        this.priorTrace = trace;
    }
    public void setNextTrace(@NotNull Trace nextTrace) {
        this.nextTrace = nextTrace;
        this.current = Math.max(this.nextTrace.current, this.current);
    }

    public int distanceToBattery(){
        if(this instanceof BatteryNode)
            return 0;
        assert this.priorTrace != null;
        assert this.priorTrace.start != null;
        return 1+ this.priorTrace.start.distanceToBattery();
    }

    public void setPriorResistivity(double voltage, double minResistivity, double maxResistivity) throws Exception {
        double idealResistance = this.getIdealResistance(voltage);
        double minResistance = (minResistivity*this.priorTrace.length) / (this.priorTrace.maxWidth * this.priorTrace.maxHeight);
        double maxResistance = maxResistivity*this.priorTrace.length / (Trace.MINWIDTH * Trace.MINHEIGHT);
        LinearProgramSolver solver = SolverFactory.newDefault();//todo solver takes time to set up and could probably be a class variable
        if(this.nextTrace == null){//leaf
            double[] objective = {idealResistance, -1.0};// e = v/I - R
            LinearProgram lp = new LinearProgram(objective);
            lp.setMinProblem(true);
            lp.addConstraint(new LinearEqualsConstraint(new double[]{1, 0}, 1, "const"));
            lp.addConstraint(new LinearBiggerThanEqualsConstraint(objective, 0, "objectiveFloor"));
            lp.addConstraint(new LinearSmallerThanEqualsConstraint(new double[]{0, 1}, maxResistance, "max"));
            lp.addConstraint(new LinearBiggerThanEqualsConstraint(new double[]{0,1}, minResistance, "min"));
            double[] solution = solver.solve(lp);
            if(solution == null)
                throw new Exception("No Solution under these constraints");
            assert solution[0] == 1.0;
            this.priorTrace.targetResistance = solution[1];

        }else {
            assert this.nextTrace.targetResistance > 0;
            double[] objective = {this.nextTrace.targetResistance, -1.0};// e = R_t - R
            LinearProgram lp = new LinearProgram(objective);
            lp.setMinProblem(true);
            lp.addConstraint(new LinearEqualsConstraint( new double[]{1, 0}, 1, "const"));
            lp.addConstraint(new LinearBiggerThanEqualsConstraint(objective, 0, "objectiveFloor"));
            lp.addConstraint(new LinearBiggerThanEqualsConstraint(new double[]{0,1}, minResistance, "min"));
            lp.addConstraint((new LinearSmallerThanEqualsConstraint(new double[]{0,1}, maxResistance, "max")));
            lp.addConstraint(new LinearSmallerThanEqualsConstraint(new double[]{0,1}, idealResistance, "ideal"));
            double[] solution = solver.solve(lp);
            if(solution == null)
                throw new Exception("No Solution under these constraints");
            assert solution[0] == 1.0;
            this.priorTrace.targetResistance = solution[1];
        }
    }

    double getIdealResistance(double voltage) {
        return voltage / (this.priorTrace.current) -this.priorTrace.additionalResistance;
    }

    @Override
    public String toString() {
        return this.name;
    }

    @Override
    public int compareTo(TraceNode o) {
        return Integer.compare(this.distanceToBattery(), o.distanceToBattery());
    }
}
