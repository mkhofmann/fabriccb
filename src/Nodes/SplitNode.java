package Nodes;

import Traces.Trace;
import com.sun.istack.internal.NotNull;
import scpsolver.constraints.LinearBiggerThanEqualsConstraint;
import scpsolver.constraints.LinearEqualsConstraint;
import scpsolver.constraints.LinearSmallerThanEqualsConstraint;
import scpsolver.lpsolver.LinearProgramSolver;
import scpsolver.lpsolver.SolverFactory;
import scpsolver.problems.LinearProgram;

public class SplitNode extends TraceNode {
    public Trace branchTrace = null;
    public SplitNode(@NotNull String name) {
        super(0, name);
    }

    @Override
    public void setNextTrace(@NotNull Trace nextTrace) {
        this.current+= nextTrace.current;
        if(this.nextTrace == null)
            super.setNextTrace(nextTrace);
        else
            this.branchTrace = nextTrace;
    }


    @Override
    public void setPriorResistivity(double voltage, double minResistivity, double maxResistivity) throws Exception {
        double length = this.priorTrace.length;
        double idealResistance = this.getIdealResistance(voltage);;
        double minResistance = minResistivity / (this.priorTrace.maxWidth * this.priorTrace.maxHeight);
        assert this.nextTrace.targetResistance > 0;
        assert this.branchTrace.targetResistance >0;
        double branchImpedance = (this.branchTrace.targetResistance *this.nextTrace.targetResistance) / (this.branchTrace.targetResistance +this.nextTrace.targetResistance);
        double[] objective = {1.0/length, ((0.0-1.0)/length) * branchImpedance};
        LinearProgram lp = new LinearProgram(objective);
        lp.setMinProblem(true);
        lp.addConstraint(new LinearEqualsConstraint( new double[]{0, 1}, 1, "const"));
        lp.addConstraint(new LinearBiggerThanEqualsConstraint(new double[]{1,0}, minResistance, "min"));
        lp.addConstraint((new LinearSmallerThanEqualsConstraint(new double[]{1,0}, maxResistivity, "max")));
        lp.addConstraint(new LinearBiggerThanEqualsConstraint(new double[]{1,0}, idealResistance, "ideal"));
        lp.addConstraint(new LinearBiggerThanEqualsConstraint(objective, 0, "objectiveFloor"));
        LinearProgramSolver solver = SolverFactory.newDefault();
        double[] solution = solver.solve(lp);
        if(solution == null)
            throw new Exception("No Solution under these constraints");
        assert solution[1] == 1.0;
        this.priorTrace.targetResistance = solution[0];
    }

}
