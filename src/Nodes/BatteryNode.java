package Nodes;

public class BatteryNode extends TraceNode {

    public static final String BATTERY = "Battery";

    public BatteryNode() {
        super(Double.MAX_VALUE, BATTERY);
    }

    @Override
    public void setPriorResistivity(double voltage, double minResistivity, double maxResistivity) {}
}
