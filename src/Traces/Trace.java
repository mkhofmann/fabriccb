package Traces;

import Materials.Conductor;
import Materials.ConductorSet;
import Materials.MetaConductor;
import Nodes.BatteryNode;
import Nodes.SplitNode;
import Nodes.TraceNode;
import com.sun.istack.internal.NotNull;
import scpsolver.constraints.LinearBiggerThanEqualsConstraint;
import scpsolver.constraints.LinearEqualsConstraint;
import scpsolver.constraints.LinearSmallerThanEqualsConstraint;
import scpsolver.lpsolver.LinearProgramSolver;
import scpsolver.lpsolver.SolverFactory;
import scpsolver.problems.LinearProgram;

import java.util.*;

public class Trace {
    public static final double MINWIDTH = .5;
    public static final int MINHEIGHT = 1;
    public int maxHeight = 4;
    public int additionalResistance = 0;
    public void setMaxHeight(int maxHeight){
        assert maxHeight>=1;
        this.maxHeight = maxHeight;
    }
    private static double WIDTH_WEIGHT = 1.0;
    private static double HEIGHT_WEIGHT = 1.0;

    public TraceNode start;
    public TraceNode end;

    public double targetResistance = -1;
    private double targetConductance(){
        return 1.0 / this.targetResistance;
    }
    public double current;
    public double length;
    public Conductor conductor = null;

    public double maxWidth;
    public double width = 1;
    public int height = 1;

    public double angleInRads;


    public Trace(@NotNull TraceNode start, @NotNull TraceNode end, double maxWidth, double length, double angleInRads) {
        assert !(end instanceof BatteryNode);
        assert length>0;
        assert maxWidth> MINWIDTH;
        this.end = end;
        this.length = length;
        this.current = end.current;
        this.maxWidth = maxWidth;
        this.start = start;
        this.angleInRads = angleInRads;
    }

    public double getDescendentCurrent(){
        double descendantCurrent = 0;
        if(this.end.nextTrace != null)
            descendantCurrent+= this.end.nextTrace.getDescendentCurrent();
        if(this.end instanceof SplitNode)
            descendantCurrent+= ((SplitNode)this.end).branchTrace.getDescendentCurrent();
        this.current = Math.max(descendantCurrent, this.end.current);
        return this.current;
    }

    private double getResistance(double w, double h, double resistivityPerVoxel){
        return (this.length*resistivityPerVoxel) /(w*h);
    }

    public double getResistance() {
        return this.getResistance(this.width, this.height, this.getResistivity());
    }

    public double getResistivity(){
        return this.conductor.getResistivity(this.angleInRads);
    }

    public double getConductance(){
        return this.conductor.getConductance(this.angleInRads);
    }

    public void setByTargetResistance(@NotNull ConductorSet conductors){
        TreeMap<Double, Conductor> conductorsByCost = new TreeMap<>();
        HashMap<Conductor, Integer> conductorsToHeight = new HashMap<>();
        HashMap<Conductor, Double> conductorsToWidth = new HashMap<>();
        for(Conductor conductor: conductors){
            double targetArea = (this.length* conductor.getResistivity(this.angleInRads)) / this.targetResistance;//l*rho / rho_t
            double bestCost = Double.MAX_VALUE;
            int bestHeight = -1;
            double bestWidth= -1;
            for(int estimateHeight = MINHEIGHT; estimateHeight<= maxHeight; estimateHeight++){
                double estimateWidth = targetArea/(double)estimateHeight;
                if(estimateWidth>=MINWIDTH && estimateWidth<=this.maxWidth){
                    double widthCost = WIDTH_WEIGHT * estimateWidth;
                    double heightCost = HEIGHT_WEIGHT * ((double) estimateHeight) * conductors.rankCost(conductor);
                    double actualResistance = (this.length * conductor.getResistivity(this.angleInRads)) / (estimateWidth * (double)estimateHeight);
                    double resistanceDif = this.targetResistance - actualResistance;
                    double cost = widthCost
                            + heightCost
                            + resistanceDif;
                    if(cost<bestCost){
                        bestHeight = estimateHeight;
                        bestWidth = estimateWidth;
                    }
                }
            }
            if(bestHeight>0){
                conductorsByCost.put(bestCost, conductor);
                conductorsToHeight.put(conductor, bestHeight);
                conductorsToWidth.put(conductor, bestWidth);
            }
        }
        this.conductor = conductorsByCost.firstEntry().getValue();
        this.width = conductorsToWidth.get(conductor);
        this.height = conductorsToHeight.get(conductor);

        double widthCost = WIDTH_WEIGHT * this.width;
        double heightCost = HEIGHT_WEIGHT * ((double) this.height) * conductors.rankCost(this.conductor);
        double actualResistance = (this.length * conductor.getResistivity(this.angleInRads)) / (this.width * (double)this.height);
        double resistanceDif = this.targetResistance - actualResistance;
        System.out.println(this.traceName()+
            String.format(" wC= %.1f hC= %.1f rDif= %.1f", widthCost, heightCost, resistanceDif));
    }
    public void buildTraceWithMetaMaterial(@NotNull ConductorSet conductors, double widthStep){
        LinearProgramSolver solver  = SolverFactory.newDefault();
        double minCost = Double.MAX_VALUE;
        double bestWidth = -1;
        LinkedList<Conductor> bestMetaMaterial = new LinkedList<>();
        double[] objective = conductors.rankedSumVector();
        double[] widthConst = new double[objective.length];
        widthConst[objective.length-1] = 1.0;
        boolean[] isInteger = new boolean[objective.length];
        double[] heightConstraint = new double[objective.length];
        ArrayList<double[]> nonNegatives = new ArrayList<>(objective.length);
        for(int i= 0; i<heightConstraint.length-1; i++) {
            isInteger[i] = true;
            heightConstraint[i] =1;//sum of all weights must be less than max height
            double[] vector = new double[objective.length];
            vector[i] = 1.0;
            nonNegatives.add(vector);
        }
        double bestLpCost = -1;
        for(double w = 1.0; w<this.maxWidth; w+= widthStep) {
            objective[objective.length-1] = w*WIDTH_WEIGHT;
            LinearProgram lp = new LinearProgram(objective);
            lp.setIsinteger(isInteger);
            lp.setMinProblem(true);
            lp.addConstraint(new LinearSmallerThanEqualsConstraint(heightConstraint, (double) this.maxHeight, "maxLayers"));
            lp.addConstraint(new LinearEqualsConstraint(widthConst, 1.0, "widthConst"));
            lp.addConstraint(new LinearBiggerThanEqualsConstraint(heightConstraint, MINHEIGHT, "minLayers"));
            int counter =0;
            for(double[] vector: nonNegatives) {
                lp.addConstraint(new LinearBiggerThanEqualsConstraint(vector, 0.0, "nonNegative_"+counter));
                counter++;
            }
            double[] conductivities = new double[objective.length];
            Iterator<Conductor> conductorIterator = conductors.iterator();
            Conductor c = conductorIterator.next();
            for(int i=0; i<conductivities.length-1; i++) {
                double conductivity = c.getConductance(this.angleInRads);
                conductivities[i] = conductivity;
                if(conductorIterator.hasNext())
                    c = conductorIterator.next();
            }
            double targetConductivity = this.targetConductance() *(this.length/w);
            lp.addConstraint(new LinearBiggerThanEqualsConstraint(conductivities, targetConductivity, "conductanceConstraint"));
            double[] sol = solver.solve(lp);
            if(sol != null) {
                LinkedList<Conductor> materials = new LinkedList<>();
                Iterator<Conductor> conductorIterator1 = conductors.iterator();
                Conductor conductor = conductorIterator1.next();
                assert sol[objective.length-1] == 1.0;
                for (double solI : sol) {
                    for (int layer = 0; layer < solI; layer++)
                        materials.add(conductor);
                    if (conductorIterator1.hasNext())
                        conductor = conductorIterator1.next();
                    else
                        break;
                }
                double lpCost = lp.evaluate(sol)*HEIGHT_WEIGHT;
                double actualResistance = 0;
                for(Conductor material: materials)
                    actualResistance+= material.getConductance(this.angleInRads);
                actualResistance = actualResistance * (w/this.length);
                double resistanceDif = this.targetResistance - actualResistance;
                double cost = lpCost + resistanceDif;
                if (cost < minCost) {
                    bestWidth = w;
                    bestMetaMaterial = materials;
                    minCost = cost;
                    bestLpCost = lpCost;
                }
            }
        }
        this.conductor = new MetaConductor(bestMetaMaterial, "metaConductor");
        this.width = (this.length* this.getResistivity()) / this.targetResistance;
        System.out.println(this.traceName()+
            String.format(" wC= %.1f lpC= %.1f h=%d", WIDTH_WEIGHT*this.width, bestLpCost-(WIDTH_WEIGHT * bestWidth), bestMetaMaterial.size()));

    }


    @Override
    public String toString() {
        return this.traceName()+ String.format(" l:%.1f W:%.1f c:%.1f", this.length, this.maxWidth, this.current);
    }

    private String traceName(){
        return String.format("%s->%s", this.start.name, this.end.name);
    }

}
